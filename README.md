# README #

This is a CodeBlocks project to compare running time for matrix multiplication of three linear algebra library (Armadillo, Eigen, NewMat).

### Compilation flags ###
* -larmadillo
* -lblas
* -llapack
* -msse2
* -O2