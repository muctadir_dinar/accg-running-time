#include <iostream>
#include "ArmadilloBench.h"
#include "EigenBench.h"
#include "NewMatBench.h"
#include "Dimension.h"
#include <list>

using namespace std;

list< Dimension > getDimensions(int start) {

    list< Dimension > ds;
    int seed = start/10*2;
    int sec = start;

    for (int i = 0; i < 4; i++) {
        ds.push_back(Dimension(start, sec));
        start = sec;
        sec = sec - seed;
    }
    return ds;
}

int main() {

    list< list< Dimension > > dimensions;

    dimensions.push_front(getDimensions(100));
    dimensions.push_front(getDimensions(50));
    dimensions.push_front(getDimensions(10));

    NewMatBench n;
    ArmadilloBench a;
    EigenBench e;

    for (list<Dimension> ds : dimensions) {
        cout << ds.front().Gethight() << "X" << ds.front().Getwidth() << endl;

        cout << n.getRunningTime(ds) << endl;
        cout << a.getRunningTime(ds) << endl;
        cout << e.getRunningTime(ds) << endl;
    }


    return 0;
}
