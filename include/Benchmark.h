#ifndef BENCHMARK_H
#define BENCHMARK_H

#include <iostream>
#include "Dimension.h"
#include <list>
using namespace std;

template <class TMat>
class Benchmark {
public:
    Benchmark();
    virtual ~Benchmark();
    virtual void printLibInfo() = 0;
    virtual TMat getMatrix(int r, int c) = 0;
    virtual TMat performMultiplication(TMat A, TMat B, TMat C, TMat D) {
        return A * B * C * D;
    };
    double getRunningTime(list<Dimension> ds);
protected:
private:
};


template <class TMat>
Benchmark<TMat>::Benchmark() {
    //ctor
}

template <class TMat>
Benchmark<TMat>::~Benchmark() {
    //dtor
}

template <class TMat>
double Benchmark<TMat>::getRunningTime(list<Dimension> ds) {
    printLibInfo();

    int n = 10;
    double totalTime = 0;
    int times = 50;

    Dimension d = ds.front();
    ds.pop_front();
    TMat A = getMatrix(d.Gethight(), d.Getwidth());
    d = ds.front();
    ds.pop_front();
    TMat B = getMatrix(d.Gethight(), d.Getwidth());
    d = ds.front();
    ds.pop_front();
    TMat C = getMatrix(d.Gethight(), d.Getwidth());
    d = ds.front();
    ds.pop_front();
    TMat D = getMatrix(d.Gethight(), d.Getwidth());

    for (int t = 0; t < times; t++) {
        int start_s=clock();

        for (int i = 0; i < n ; i++) {
            TMat res = performMultiplication(A, B, C, D);
        }

        int stop_s=clock();
        double timeMs = (stop_s-start_s)/double(CLOCKS_PER_SEC)*1000;
        totalTime += timeMs;
        // cout << "time: " << timeMs << "ms" << endl;
    }

    return totalTime/times/n;
}

#endif // BENCHMARK_H
