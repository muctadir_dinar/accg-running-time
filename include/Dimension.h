#ifndef DIMENSION_H_INCLUDED
#define DIMENSION_H_INCLUDED

class Dimension {
public:
    Dimension(unsigned int height, unsigned int width) {
        this->height = height;
        this->width = width;
    }
    unsigned int Gethight() {
        return height;
    }
    void Setheight(unsigned int val) {
        height = val;
    }
    unsigned int Getwidth() {
        return width;
    }
    void Setwidth(unsigned int val) {
        width = val;
    }
protected:
private:
    unsigned int height;
    unsigned int width;
};
#endif // DIMENSION_H_INCLUDED
