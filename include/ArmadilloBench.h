#ifndef ARMADILLO_H_INCLUDED
#define ARMADILLO_H_INCLUDED

#include "Benchmark.h"
#include <armadillo>

using namespace arma;

class ArmadilloBench : public Benchmark<mat> {
public:
    virtual mat getMatrix(int r, int c) {
        return randu(r, c);
    };

    virtual void printLibInfo() {
        cout << "Armadillo version: " << arma_version::as_string() << endl;
    };
};

#endif // ARMADILLO_H_INCLUDED
