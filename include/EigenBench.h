#ifndef EIGEN_H_INCLUDED
#define EIGEN_H_INCLUDED

#include "Benchmark.h"
#include <eigen3/Eigen/Dense>

using namespace Eigen;

class EigenBench : public Benchmark<MatrixXd> {
public:
    virtual MatrixXd getMatrix(int r, int c) {
        return MatrixXd::Random(r, c);
    };

    virtual void printLibInfo() {
        cout << "Eigen version: " << EIGEN_WORLD_VERSION << "." << EIGEN_MAJOR_VERSION << "." << EIGEN_MINOR_VERSION<< endl;
    };
};

#endif // EIGEN_H_INCLUDED
