#ifndef NEWMAT_H_INCLUDED
#define NEWMAT_H_INCLUDED

#include "Benchmark.h"
#include <newmat/newmat.h>
#include <newmat/newmatio.h>
#include <iomanip>

class NewMatBench : public Benchmark<NEWMAT::Matrix> {
public:
    virtual NEWMAT::Matrix getMatrix(int r, int c) {
        return randomMatrix(r, c);
    };

    virtual void printLibInfo() {
        cout << "NEWMAT" << endl;
    };

private:
    NEWMAT::Matrix randomMatrix(int r, int c) {
        int size = c * r;
        double *array = new double[size];
        float X = 1;

        for (int i = 0; i < size; i++) {
            array[i] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/X));
        }

        NEWMAT::Matrix mat(r, c);
        mat << array;
        return mat;
    }
};

#endif // NEWMAT_H_INCLUDED
